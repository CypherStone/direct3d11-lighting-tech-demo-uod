#pragma once
#include "stdafx.h"

#define DINPUT_VERSION 0x0800

class DirectInputManager
{

private:
	IDirectInput8* directInput;
	IDirectInputDevice8* keyboard;
	IDirectInputDevice8* mouse;

	unsigned char keyboardState[256];
	unsigned char previous[256];
	DIMOUSESTATE mouseState;

	int screenWidth;
	int screenHeight;
	int mouseX;
	int mouseY;
	bool start;
	void Initiate(void);
	bool UpdateKeyboard(void);
	bool UpdateMouse(void);

public:
	DirectInputManager(void);
	~DirectInputManager(void);

	bool SetupInput(HINSTANCE hInstance, HWND hWnd, int width, int height);
	bool IsKeyDown(int key);
	bool Update(void);
	
	int GetMouseX(void);
	int GetMouseY(void);
	void ReleaseResources(void);
};

