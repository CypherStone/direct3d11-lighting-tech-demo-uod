#pragma once
#include "stdafx.h"

class DirectDepthShader
{

private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	ID3D11InputLayout* layout;
	ID3D11Buffer* matrixBuffer;
	ID3D11VertexShader* vertexShader;
	ID3D11PixelShader* pixelShader;

	void Initiate(void);
	void RenderShader(ID3D11DeviceContext *deviceContext, int vertexCount);
public:

	DirectDepthShader(void);
	DirectDepthShader(const DirectDepthShader &light);
	~DirectDepthShader(void);

	bool SetupShader(ID3D11Device* device, char* vShader, char* pShader);

	bool Render(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, int vertexCount);

	void ReleaseResources(void);
};
