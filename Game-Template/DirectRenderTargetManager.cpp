#include "stdafx.h"

DirectRenderTargetManager::DirectRenderTargetManager(void) 
{ 
	downSamplerRender = 0;
	upSamplerRender = 0;
	horizontalBlurRender = 0;
	verticalBlurRender = 0;
}

DirectRenderTargetManager::DirectRenderTargetManager(const DirectRenderTargetManager& other) { }

DirectRenderTargetManager::~DirectRenderTargetManager(void) { }

bool DirectRenderTargetManager::SetupRenderManager(ID3D11Device *device, int mapWidth, int mapHeight, float screenNear, float screenFar)
{
	int downSampleWidth = mapWidth / 2;
	int downSampleHeight = mapHeight / 2;

	depthRender = new DirectRenderTarget;
	if(depthRender)
	{
		if(!depthRender->SetupRenderTarget(device, mapWidth, mapHeight, D3DXVECTOR2(screenNear, screenFar)))
			return false;
	}
	else
		return false;

	downSamplerRender = new DirectRenderTarget;
	if(downSamplerRender)
	{
		if(!downSamplerRender->SetupRenderTarget(device, downSampleWidth, downSampleHeight, D3DXVECTOR2(1.0f, screenFar)))
			return false;
	}
	else
		return false;

	horizontalBlurRender = new DirectRenderTarget;
	if(horizontalBlurRender)
	{
		if(!horizontalBlurRender->SetupRenderTarget(device, downSampleWidth, downSampleHeight, D3DXVECTOR2(0.1f, screenFar)))
			return false;
	}
	else
		return false;

	verticalBlurRender = new DirectRenderTarget;
	if(verticalBlurRender)
	{
		if(!verticalBlurRender->SetupRenderTarget(device, downSampleWidth, downSampleHeight, D3DXVECTOR2(0.1f, screenFar)))
			return false;
	}
	else
		return false;

	upSamplerRender = new DirectRenderTarget;
	if(upSamplerRender)
	{
		if(!upSamplerRender->SetupRenderTarget(device, mapWidth, mapHeight, D3DXVECTOR2(1.0f, screenFar)))
			return false;
	}
	else
		return false;

	return true;
}

DirectRenderTarget * DirectRenderTargetManager::DepthRender(void)
{
	return depthRender;
}

DirectRenderTarget * DirectRenderTargetManager::DownSampleRender(void)
{
	return downSamplerRender;
}

DirectRenderTarget * DirectRenderTargetManager::UpSampleRender(void)
{
	return upSamplerRender;
}

DirectRenderTarget * DirectRenderTargetManager::HorizontalBlurRender(void)
{
	return horizontalBlurRender;
}

DirectRenderTarget * DirectRenderTargetManager::VerticalBlurRender(void)
{
	return verticalBlurRender;
}

void DirectRenderTargetManager::ReleaseResources(void)
{
	GameEngine::SafeReleaseDelete(downSamplerRender);
	GameEngine::SafeReleaseDelete(depthRender);
	GameEngine::SafeReleaseDelete(upSamplerRender);
	GameEngine::SafeReleaseDelete(horizontalBlurRender);
	GameEngine::SafeReleaseDelete(verticalBlurRender);
}