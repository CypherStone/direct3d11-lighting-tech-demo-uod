cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer CameraBuffer
{
	float3 camera;
	float padding;
};

struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
    float3 binormal : BINORMAL;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
    float4 viewPosition : TEXCOORD1;
	float3 viewDirection : TEXCOORD2;
	float4 worldPosition : TEXCOORD3;
};

PixelInputType SoftShadowVertexShader(VertexInputType input)
{
	PixelInputType output;    
    input.position.w = 1.0f;

	output.worldPosition = mul(input.position, worldMatrix);

    output.position = output.worldPosition;
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    output.viewPosition = output.position;
    output.tex = input.tex;
    
    output.normal = mul(input.normal, (float3x3)worldMatrix);
    output.normal = normalize(output.normal);

	output.viewDirection = camera.xyz - output.worldPosition.xyz;
	output.viewDirection = normalize(output.viewDirection);

    output.tangent = mul(input.tangent, (float3x3)worldMatrix);
    output.tangent = normalize(output.tangent);

    output.binormal = mul(input.binormal, (float3x3)worldMatrix);
    output.binormal = normalize(output.binormal);

	return output;

}