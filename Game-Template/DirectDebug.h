#pragma once
#include "stdafx.h"
#include "RenderEngine.h"

class DirectDebug
{
private:
	IFW1Factory *debugDFactory;
	IFW1FontWrapper *debugDisplay;
	double fps;
	DWORD frames;
	DWORD currentTime;
	DWORD lastUpdate;
public:
	void ComputeFrameRate(void);
	void DebugTextDisplay(Direct3DManager *device, Camera *camera, RenderEngine::RenderType renderType, int polys);
	void SetupDisplayFont(ID3D11Device *device, ID3D11DeviceContext *deviceContext);
	void ReleaseResources(void);
};