#include "stdafx.h"
#include "DirectInputManager.h"

void DirectInputManager::Initiate(void)
{
	directInput = 0;
	keyboard = 0;
	mouse = 0;

	screenWidth = 0;
	screenHeight = 0;
	mouseX = 0;
	mouseY = 0;
}

DirectInputManager::DirectInputManager(void)
{
	Initiate();
}

DirectInputManager::~DirectInputManager(void)
{
}

bool DirectInputManager::SetupInput(HINSTANCE hInstance, HWND hWnd, int width, int height)
{
	HRESULT result;
	screenWidth = width;
	screenHeight = height;

	result = DirectInput8Create(hInstance, DINPUT_VERSION, IID_IDirectInput8, (void**)&directInput, NULL);
	if(FAILED(result))
	{
		return false;
	}

	result = directInput->CreateDevice(GUID_SysKeyboard, &keyboard, NULL);
	if(FAILED(result))
	{
		return false;
	}

	result = keyboard->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(result))
	{
		return false;
	}

	// This makes sure that the keyboard input is exclusive to this application only.
	result = keyboard->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(result))
	{
		return false;
	}

	result = keyboard->Acquire();
	if(FAILED(result))
	{
		return false;
	}

	result = directInput->CreateDevice(GUID_SysMouse, &mouse, NULL);
	if(FAILED(result))
	{
		return false;
	}

	result = mouse->SetDataFormat(&c_dfDIMouse);
	if(FAILED(result))
	{
		return false;
	}

	result = mouse->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(result))
	{
		return false;
	}

	result = mouse->Acquire();
	if(FAILED(result))
	{
		return false;
	}


	return true;
}

bool DirectInputManager::Update(void)
{
	if(!UpdateMouse())
	{
		return false;
	}

	if(!UpdateKeyboard())
	{
		return false;
	}

	return true;
}

bool DirectInputManager::UpdateKeyboard(void)
{
	HRESULT result;
	

	result = keyboard->GetDeviceState(256, &keyboardState);
	if(FAILED(result))
	{
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			keyboard->Acquire();
		}
		else
		{
			return false;
		}
	}
	return true;
}

bool DirectInputManager::UpdateMouse(void)
{
	HRESULT result;

	result = mouse->GetDeviceState(sizeof(DIMOUSESTATE), &mouseState);
	if(FAILED(result))
	{
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			mouse->Acquire();
		}
		else
		{
			return false;
		}
	}

	mouseX = mouseState.lX;
	mouseY = mouseState.lY;
	SetCursorPos(screenWidth / 2, screenHeight / 2);

	return true;
}

int DirectInputManager::GetMouseX(void)
{
	return mouseX;
}

int DirectInputManager::GetMouseY(void)
{
	return mouseY;
}

bool DirectInputManager::IsKeyDown(int key)
{
	if(keyboardState[key] & 0x80)
	{
		return true;
	}

	return false;
}

void DirectInputManager::ReleaseResources(void)
{
	if(mouse)
	{
		mouse->Unacquire();
		mouse->Release();
		mouse = 0;
	}

	if(keyboard)
	{
		keyboard->Unacquire();
		keyboard->Release();
		keyboard = 0;
	}

	GameEngine::SafeRelease(directInput);
}

