#pragma once
#include "stdafx.h"

enum Controls
{
	NOINPUT,
	MOUSE,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

class InputControl
{
	private:
		FLOAT _mouseX;
		FLOAT _mouseY;
		Controls _controls;
		FLOAT _movement;
		void Init(void);
		void Copy(const InputControl &input);

	public:
		InputControl(void);
		InputControl(const InputControl &input);
		~InputControl(void);

		void InputUpdate(Camera &camera);
		void GetInput(const HWND &hWnd, const UINT &msg, const WPARAM &wParam, const LPARAM &lParam);
};