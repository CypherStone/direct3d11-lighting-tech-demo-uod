#pragma once
#include "stdafx.h"

class DirectShadowShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
		D3DXMATRIX lightView;
		D3DXMATRIX lightProjection;
	};

	struct LightProperties
	{
		D3DXVECTOR4 ambient;
	};

	struct LightBufferType2
	{
		D3DXVECTOR3 lightPosition;
		float padding;
	};


private:
	ID3D11VertexShader* vertexShader;
	ID3D11PixelShader* pixelShader;
	ID3D11InputLayout* layout;
	ID3D11SamplerState* sampleStateClamp;
	ID3D11Buffer* matrixBuffer;
	ID3D11Buffer* lightBuffer;
	ID3D11Buffer* lightBuffer2;

	void RenderShader(ID3D11DeviceContext*, int);

public:
	DirectShadowShader(void);
	DirectShadowShader(const DirectShadowShader&);
	~DirectShadowShader(void);

	bool SetupShader(ID3D11Device* device, char* vShader, char* pShader);

	bool Render(ID3D11DeviceContext* deviceContext, int vertexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DXMATRIX lightViewMatrix, D3DXMATRIX lightProjectionMatrix, ID3D11ShaderResourceView* depthMapTexture, D3DXVECTOR3 lightPosition, D3DXVECTOR4 ambient);
	void ReleaseResources(void);
};