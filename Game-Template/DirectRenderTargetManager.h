#pragma once
#include "stdafx.h"

class DirectRenderTargetManager
{
private:
	DirectRenderTarget *depthRender;
	DirectRenderTarget *downSamplerRender;
	DirectRenderTarget *upSamplerRender;
	DirectRenderTarget *horizontalBlurRender;
	DirectRenderTarget *verticalBlurRender;

public:
	DirectRenderTargetManager(void);
	DirectRenderTargetManager(const DirectRenderTargetManager& other);
	~DirectRenderTargetManager(void);

	bool SetupRenderManager(ID3D11Device *device, int mapWidth, int mapHeight, float screenNear, float screenFar);
	void ReleaseResources(void);

	DirectRenderTarget *DepthRender(void);
	DirectRenderTarget *DownSampleRender(void);
	DirectRenderTarget *UpSampleRender(void);
	DirectRenderTarget *HorizontalBlurRender(void);
	DirectRenderTarget *VerticalBlurRender(void);
};

