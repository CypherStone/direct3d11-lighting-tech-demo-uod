#pragma once
#include "stdafx.h"

class GameEngine
{
	private:
		HWND hWnd;
		Camera *camera;
		Direct3DManager *d3DDevice;
		DirectInputManager *inputManager;
		RenderEngine *renderEngine;
		DirectDebug *directDebug;

		RenderEngine::RenderType renderType;


		bool lightAnimation1;
		bool lightAnimation2;
		bool gameRunning;
		bool changeRenderPlus;
		bool changeRenderMinus;
		bool changeFullscreen;
		std::vector<Object3D> *objects;
		std::vector<DirectLight> *lights;

		void Initiate(void);
		void Copy(const GameEngine &game);
		void LoadModels(void);
		void SetupLights(void);

		void UpdateGame(void);
		void RenderGame(void);

	public:
		GameEngine(void);
		GameEngine(const GameEngine &game);
		~GameEngine(void);

		bool SetupEngine(HINSTANCE hInstance, HWND hwnd, UINT width, UINT height);
		void GameLoop(void);
		void UpdateScreen(UINT width, UINT height);
		void ReleaseResources(void);

		inline bool IsGameRunning(void)
		{
			return gameRunning;
		}

		template<typename T> static inline void SafeRelease(T*& a)
		{
			if(a)
			{
				a->Release();
				a = 0;
			}
		}
		template<typename T> static inline void SafeReleaseDelete(T*& a)
		{
			if(a)
			{
				a->ReleaseResources();
				delete a;
				a = 0;
			}
		}
		template<typename T> static inline void SafeDelete(T*& a) 
		{
			if(a)
			{
				delete a;
				a = 0;
			}
		}
};
