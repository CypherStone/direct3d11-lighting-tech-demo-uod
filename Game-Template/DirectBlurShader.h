#pragma once
#include "stdafx.h"

class DirectBlurShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct ScreenSizeBufferType
	{
		float screenHeight;
		D3DXVECTOR2 padding;
		bool vertical;
	};

	ID3D11VertexShader* vertexShader;
	ID3D11PixelShader* pixelShader;
	ID3D11InputLayout* layout;
	ID3D11SamplerState* sampleState;
	ID3D11Buffer* matrixBuffer;
	ID3D11Buffer* screenSizeBuffer;
	
	void RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount);

public:
	DirectBlurShader(void);
	DirectBlurShader(const DirectBlurShader&);
	~DirectBlurShader(void);

	bool SetupShader(ID3D11Device* device, char* vShader, char* pShader);
	bool Render(ID3D11DeviceContext* deviceContext, int vertexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
									 D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture, bool vertical, float screenSize);

	void ReleaseResources(void);
};
