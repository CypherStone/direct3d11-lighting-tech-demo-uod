#include "stdafx.h"

void GameEngine::Initiate(void)
{
	hWnd = NULL;
	d3DDevice = 0;
	renderEngine = 0;
	inputManager = 0;
	directDebug = 0;

	camera = 0;
	lights = 0;

	gameRunning = false;
}

GameEngine::GameEngine(void)
{
	Initiate();
}

GameEngine::GameEngine(const GameEngine &game) { }
		
GameEngine::~GameEngine(void) { }

bool GameEngine::SetupEngine(HINSTANCE hInstance, HWND hwnd, UINT width, UINT height)
{
	int shadowMapHeight = 1024;
	int shadowMapWidth = 1024;

	hWnd = hwnd;
	renderType = RenderEngine::SHADOWMAP;
	camera = new Camera;
	if(camera)
	{
		camera->Initiate(width, height, 45.0f, 1.0f, 7500.0f, D3DXVECTOR3(0.0f, 0.0f, -10.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		camera->GenerateBaseView();
		camera->YPosition(50.0f);
	}
	else
		return false;

	// Setup the Direct3D Manager.
	d3DDevice = new Direct3DManager;
	if(d3DDevice)
	{
		if(!d3DDevice->SetupDirectX11(hWnd, *camera, true, false))	
			return false;
	}
	else 
		return false;
	
	renderEngine = new RenderEngine;
	if(renderEngine)
	{
		if(!renderEngine->SetupEngine(d3DDevice, 1024, 1024))
			return false;
	}
	else
		return false;

	// Setup the input manager for the engine.
	inputManager = new DirectInputManager;
	if(inputManager)
	{
		if(!inputManager->SetupInput(hInstance, hWnd, width, height)) 
			return false;
	}
	else 
		return false;

	directDebug = new DirectDebug;
	if(directDebug)
	{
		directDebug->SetupDisplayFont(d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	}
	else 
		return false;
	
	// Loading all of the .OBJ 3D models.
	LoadModels();

	// Finally, setting up all of the lights that are in the tech demo's scene.
	SetupLights();
	
	return true;
}

void GameEngine::LoadModels(void)
{
	const int objAmount = 2;
	objects = new std::vector<Object3D>;

	//Loads models relative to the project from the string array
	std::string objLocation[objAmount] = 
	{
		".\\Models\\Castle\\Castle.obj",
		".\\Models\\Castle\\Brad.obj"
	};

	for(int models = 0; models < objAmount; models++)
	{
		Object3D obj;
		Utility::ImportOBJ(obj, objLocation[models].c_str(), d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
		if(models == 1)
		{
			obj.SetPosition(D3DXVECTOR3(-500.0f, 15.0f, 0.0f));
		}
		objects->push_back(obj);

	}
}

void GameEngine::SetupLights(void)
{
	lights = new std::vector<DirectLight>;
	
	DirectLight light;
	light.SetupLight(D3DXVECTOR4(0.1f, 0.1f, 0.1f, 1.0f), D3DXVECTOR4(0.3f, 0.3f, 1.0f, 1.0f),
		D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f), 64.0f, D3DXVECTOR3(0.0f, 30.0f, 240.0f), D3DXVECTOR3(1.0f, 1.0f, 1.0f), 10.0f);
	light.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1.0f, 100.0f));
	light.GenerateProjection(500.0f, 5.0f);

	lights->push_back(light);

	light.SetPosition(200.0f, 100.0f, -350.0f);
	light.GenerateProjection(550.0f, 5.0f);
	light.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1.0f, 100.0f));
	light.SetDiffuse(0.7f, 0.7f, 0.2f, 1.0f);
	light.SetSpecular(0.7f, 0.7f, 0.2f, 1.0f);
	lights->push_back(light);

	light.SetPosition(0.0f, 200.0f, -120.0f);
	light.GenerateProjection(2000.0f, 1.5f);
	light.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1.0f, 100.0f));
	light.SetDiffuse(0.8f, 0.2f, 0.2f, 1.0f);
	light.SetSpecular(1.0f, 0.6f, 0.6f, 1.0f);
	lights->push_back(light);
	
	light.SetPosition(-200.0f, 100.0f, 0.0f);
	light.GenerateProjection(2000.0f, 1.5f);
	light.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1.0f, 100.0f));
	light.SetDiffuse(0.2f, 0.8f, 0.2f, 1.0f);
	light.SetSpecular(0.4f, 0.4f, 0.8f, 1.0f);
	lights->push_back(light);

	light.SetPosition(0.0f, 300.0f, 200.0f);
	light.GenerateProjection(2000.0f, 1.5f);
	light.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1.0f, 100.0f));
	light.SetDiffuse(0.6f, 0.8f, 0.9f, 1.0f);
	light.SetSpecular(0.8f, 0.8f, 0.8f, 1.0f);
	lights->push_back(light);

	lights->at(1).SetLookat(-150.0f, lights->at(1).GetPosition().y, 0.0f);
	lights->at(1).GenerateView();

	lights->at(2).SetLookat(500.0f, lights->at(2).GetPosition().y,  lights->at(2).GetPosition().z);
	lights->at(2).GenerateView();
	
	light.SetPosition(155.0f, 250.0f, -270.0f);
	light.GenerateProjection(1000.0f, 1.5f);
	light.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1.0f, 100.0f));
	light.SetDiffuse(0.9f, 0.1f, 0.5f, 1.0f);
	light.SetSpecular(0.8f, 0.8f, 0.8f, 1.0f);
	lights->push_back(light);
}

void GameEngine::GameLoop(void)
{
	MSG msg;
	gameRunning = true;
	while(gameRunning)
    {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

		if(msg.message == WM_QUIT)
		{
			gameRunning = false;
		}
		else
		{
			directDebug->ComputeFrameRate();
			UpdateGame();
			RenderGame();
		}
	}
}

void GameEngine::UpdateGame(void)
{
	if(inputManager->Update())
	{
		if(inputManager->IsKeyDown(DIK_ESCAPE))
		{
			gameRunning = false;
			return;
		}

		if(inputManager->IsKeyDown(DIK_W))
			camera->ForwardMovement(5.0f, 500.0f);
		else if(inputManager->IsKeyDown(DIK_S))
			camera->BackwardMovement(5.0f, -500.0f);

		if(inputManager->IsKeyDown(DIK_A))
			camera->StrafeLeft(5.0f, 500.0f);
		else if(inputManager->IsKeyDown(DIK_D))
			camera->StrafeRight(5.0f, -500.0f);

		if(inputManager->IsKeyDown(DIK_SPACE))
			camera->InclineHeight(5.0f, 500.0f);
		else if(inputManager->IsKeyDown(DIK_LSHIFT))
			camera->DeclineHeight(5.0f, 10.0f);

		if(inputManager->IsKeyDown(DIK_NUMPADMINUS) && changeRenderMinus)
		{
			changeRenderMinus = false;
			if(renderType - 1 >= 0)
			{
				renderType = (RenderEngine::RenderType)((int)renderType - 1);
			}
		}
		else if(!inputManager->IsKeyDown(DIK_NUMPADMINUS) && !changeRenderMinus)
		{
			changeRenderMinus = true;
		}

		if(inputManager->IsKeyDown(DIK_NUMPADPLUS) && changeRenderPlus)
		{
			changeRenderPlus = false;
			if(renderType + 1 < sizeof(RenderEngine::RenderType) + 1)
			{
				renderType = (RenderEngine::RenderType)((int)renderType + 1);
			}
		}
		else if(!inputManager->IsKeyDown(DIK_NUMPADPLUS) && !changeRenderPlus)
		{
			changeRenderPlus = true;
		}

		if(inputManager->IsKeyDown(DIK_F12) && changeFullscreen)
		{
			d3DDevice->SetFullscreen();
			changeFullscreen = false;
		}
		else if(!inputManager->IsKeyDown(DIK_F12) && !changeFullscreen)
		{
			changeFullscreen = true;
		}

		camera->Update(inputManager);
	}

	float light1zAnim = lights->at(0).GetPosition().z;
	
	if(lights->at(0).GetPosition().z > 290.0f)
	{
		lightAnimation1 = false;
	}
	else if(lights->at(0).GetPosition().z < -290.0f)
	{
		lightAnimation1 = true;
	}

	if(lightAnimation1)
	{
		light1zAnim += 0.75f;
	}
	else
	{
		light1zAnim -= 0.75f;
	}

	lights->at(0).SetPosition(0.0f, lights->at(0).GetPosition().y, light1zAnim);
	if(light1zAnim != 0.0f)
	{
		lights->at(0).SetLookat(0.0f, 0.0f, 0.0f);
		lights->at(0).GenerateView();
	}

	lights->at(3).SetLookat(-700.0f, lights->at(3).GetPosition().y,  lights->at(3).GetPosition().z);
	lights->at(3).GenerateView();

	float light5yAnim = lights->at(4).GetPosition().y;	
	if(lights->at(4).GetPosition().y > 350.0f)
	{
		lightAnimation2 = false;
	}
	else if(lights->at(4).GetPosition().y < 200.0f)
	{
		lightAnimation2 = true;
	}

	if(lightAnimation2)
	{
		light5yAnim += 0.25f;
	}
	else
	{
		light5yAnim -= 0.25f;
	}

	lights->at(4).SetPosition(0.0f, light5yAnim, lights->at(4).GetPosition().z);
	lights->at(4).SetLookat(0.0f, 50.0f, 500.0f);
	lights->at(4).GenerateView();

	//lights->at(5).SetPosition(0.0f, light5yAnim, lights->at(5).GetPosition().z);
	lights->at(5).SetLookat(100.0f, 100.0f, -270.0f);
	lights->at(5).GenerateView();

	objects->at(1).YAngleAdd(0.015f);
}

void GameEngine::RenderGame(void)
{
	renderEngine->Render(d3DDevice, camera, objects, lights, directDebug, renderType);
}

void GameEngine::UpdateScreen(UINT width, UINT height)
{
	camera->SetScreen(width, height);
}

void GameEngine::ReleaseResources(void)
{
	SafeReleaseDelete(d3DDevice);
	SafeReleaseDelete(inputManager);

	for(int model = 0; model < (int)objects->size(); model++)
	{
		objects->at(model).ReleaseResources();
	}

	SafeDelete(objects);
	SafeDelete(camera);
	SafeDelete(lights);
}