#pragma once
#include "stdafx.h"

class DirectRenderTarget
{

private:
	ID3D11Texture2D* renderTargetTexture;
	ID3D11RenderTargetView* renderTargetView;
	ID3D11ShaderResourceView* shaderResourceView;
	ID3D11Texture2D* depthStencilBuffer;
	ID3D11DepthStencilView* depthStencilView;
	D3D11_VIEWPORT viewport;
	D3DXMATRIX projection;
	D3DXMATRIX ortho;

public:
	DirectRenderTarget(void);
	DirectRenderTarget(const DirectRenderTarget&);
	~DirectRenderTarget(void);

	bool SetupRenderTarget(ID3D11Device*, int width, int height, D3DXVECTOR2);
	void ReleaseResources(void);

	void SetRenderTarget(ID3D11DeviceContext* deviceContext);
	void ClearRenderTarget(ID3D11DeviceContext* deviceContext, D3DXVECTOR4 colour);

	inline ID3D11ShaderResourceView* GetShaderResourceView()
	{
		return shaderResourceView;
	}

	inline D3DXMATRIX GetProjectionMatrix(void)
	{
		return projection;
	}

	inline D3DXMATRIX GetOrthoMatrix(void)
	{
		return ortho;
	}
};
