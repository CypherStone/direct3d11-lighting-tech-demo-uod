#pragma once
#include "stdafx.h"

class DirectDiffuseShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	ID3D11VertexShader* vertexShader;
	ID3D11PixelShader* pixelShader;
	ID3D11InputLayout* layout;
	ID3D11SamplerState* sampleStateWrap;
	ID3D11Buffer* matrixBuffer;

	void RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount);

public:
	DirectDiffuseShader(void);
	DirectDiffuseShader(const DirectDiffuseShader&);
	~DirectDiffuseShader(void);

	bool SetupShader(ID3D11Device* device, char* vShader, char* pShader);
	bool Render(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, Mesh *mesh);

	void ReleaseResources(void);

};
